/* tslint:disable: no-any */
/*
Copyright 2022 mx-puppet-discord
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import { Client, Constants, TextChannel } from "@mx-puppet/better-discord.js";
import { patchAfter, patchBefore, patchClass } from "../utils/Patcher";

const { Events } = Constants;

// Fix Typescript errors
abstract class ActionClass {
	public client: Client;

	constructor(client: Client) {
		this.client = client;
	}

	public abstract handle(data: any): any;
	public getMessage(data: any, channel: TextChannel, cache: any): any {

	}
}
// @ts-ignore
const stubActionsManager = (new Client()).actions;
const ActionsManager = stubActionsManager.constructor;
const Action: typeof ActionClass = stubActionsManager.MessageCreate.__proto__.__proto__.constructor;

const actionProto = Action.prototype;

class ActionPatch {
	public getMessage = patchBefore(actionProto.getMessage, (data: any, channel: TextChannel, cache: any) => {
		if (!channel.messages) {
			// tslint:disable-next-line:no-console
			console.log("BUG-3: no messages", channel);
			return null;
		}
	});
}

patchClass(actionProto, ActionPatch);

class MessageCreateAction extends Action {
	public handle(data: any): any {
		const client = this.client;
		if (!client.channels) {
			// tslint:disable-next-line:no-console
			console.log("BUG-1: no channels", client.channels);
			return {};
		}
		const channel = client.channels.cache.get(data.channel_id) as TextChannel;
		if (channel) {
			if (!channel.messages) {
				// tslint:disable-next-line:no-console
				console.log("BUG-2: no messages", channel);
				return {};
			}
			const existing = channel.messages.cache.get(data.id);
			if (existing) { return { message: existing }; }
			const message = channel.messages.add(data);
			const user = message.author;
			const member = message.member;
			channel.lastMessageID = data.id;
			if (user) {
				user.lastMessageID = data.id;
				// @ts-ignore
				user.lastMessageChannelID = channel.id;
			}
			if (member) {
				member.lastMessageID = data.id;
				member.lastMessageChannelID = channel.id;
			}

			/**
			 * Emitted whenever a message is created.
			 * @event Client#message
			 * @param {Message} message The created message
			 */
			client.emit(Events.MESSAGE_CREATE, message);
			return { message };
		}

		return {};
	}
}

const proto = ActionsManager.prototype;

class ActionsManagerPatch {

	public register = patchAfter(proto.register, function afterRegister() {
		if (!this.imported) {
			this.MessageCreate = new MessageCreateAction(this.client);

			this.imported = true;
		}
	});
	private imported = false;
}

patchClass(proto, ActionsManagerPatch);
